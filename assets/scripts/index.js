/* ==========================================================================
   Main JavaScript File
   ========================================================================== */


///////// FastClick (removes delay on older mobile touch events) ///////
$(function() {
    FastClick.attach(document.body);
});

///////////////////////Nav Offcanvas///////////////////////
$('.trigger').click(function() {
  var $this = $(this),
  notThis = $this.hasClass('open'),
  thisNav = $this.attr("rel");
  //close if you click another menu trigger
  if (!notThis) {
    $('.triggered-area, .trigger').removeClass('open');
    if ( $( '.trigger' ).hasClass( "block-scroll" ) ){
      $('html').removeClass('disable-scroll');
      $('#overlay-mobile').removeClass('visible');
    }
  }
  //open the nav
  $this.toggleClass('open');
  $("#"+thisNav).toggleClass('open');
  if ( $( this ).hasClass( "block-scroll" ) ){
    $('html').toggleClass('disable-scroll');
    $('#overlay-mobile').toggleClass('visible');
  }
});
//close if you click on anything but this nav item or a trigger
$(document).on('click', function(event) {
  if (!$(event.target).closest('.triggered-area, .trigger').length) {
    $('.triggered-area, .trigger').removeClass('open');
    if ( $( '.trigger' ).hasClass( "block-scroll" ) ){
      $('html').removeClass('disable-scroll');
      $('#overlay-mobile').removeClass('visible');
    }
  }
});

// //Close button
// $('.offcanvas-close-button').click(function() {
//   $('.triggered-area, .trigger').removeClass('open');
//   $('html').removeClass('disable-scroll');
//   $('#overlay-mobile').removeClass('visible');
// });

///////////////////////Nav Search-Drop///////////////////////

$('.search-trigger').click(function() {
  var $this = $(this),
  notThis = $this.hasClass('open'),
  thisNav = $this.attr("rel");
  //close if you click another menu trigger
  if (!notThis) {
    $('.search-trigger').removeClass('open');
   }
  //open the nav
  $this.toggleClass('open');
  $("#"+thisNav).toggleClass('open');
});
//close if not this nav item or outside of nav fully
$(document).on('click', function(event) {
  if ($('.search-drop').hasClass('open') && !$(event.target).closest('.search-trigger, .search-drop').length) {
    $('.search-drop').removeClass('open');
    $("#"+thisNav).removeClass('open');
  }
});

///////////////////////Drop Menu///////////////////////
//open
$('.top-level-a').click(function() {
  var $this = $(this),
  notThis = $this.hasClass('open');

  if (!notThis) {
     $('.top-level-a').removeClass('open');
    $('html').removeClass('disable-scroll');
   }
  $this.toggleClass('open');
  $('html').toggleClass('disable-scroll');
});
//close if not this nav item or outside of nav fully
$(document).on('click', function(event) {
  if (!$(event.target).closest('.top-level-li').length) {
    $('.top-level-a').removeClass('open');
  }
});

///////////////////////Site Search Dropdown///////////////////////
//open
$('#site-search-trigger').click(function() {
  var $this = $(this),
  notThis = $this.hasClass('open');

  if (!notThis) {
     $('#site-search-trigger').removeClass('open');
     $('#site-search-bar').removeClass('search-open');
   }
  $this.toggleClass('open');
  $('#site-search-bar').toggleClass('search-open');
  $( "#site-search-input" ).focus();
});
//close if not this nav item or outside of nav fully
$(document).on('click', function(event) {
  if (!$(event.target).closest('#site-search-trigger, #site-search-bar').length) {
    $('#site-search-trigger').removeClass('open');
    $('#site-search-bar').removeClass('search-open');
  }
});

$('#site-search-close').click(function() {
  $('#site-search-trigger').removeClass('open');
  $('#site-search-bar').removeClass('search-open');
});

///////////////////////Tabs///////////////////////
// http://www.entheosweb.com/tutorials/css/tabs.asp

//Progressive Enhancement bits:
$("document").ready(function(){
  $(".tab-content").hide();
  $(".tab-content:first").show();
});
//The main bits
$("ul.tabs li").click(function() {
  $(".tab-content").hide();
  var activeTab = $(this).attr("rel");
  $("#"+activeTab).fadeIn();

  $("ul.tabs li").removeClass("active");
  $(this).addClass("active");
});

/////////Accordion///////
$(document).ready(function() {
      $('body').addClass('js');
      var $accordion = $('.accordion');

    $accordion.on("click", function(e){
      e.preventDefault();
      var $this = $(this);
      $this.toggleClass('active');
      $this.next('.panel').toggleClass('active');
    });
});

////////Expanding Side Nav/////////
$(document).ready(function() {
    $('body').addClass('js');
    var $menu = $('#offcanvas-nav'),
        $menulink = $('.offcanvas-menu-link'),
        $menuTrigger = $('.has-subnav > a + .nav-more-link');

  $menulink.click(function(e) {
    e.preventDefault();
    $menulink.toggleClass('active');
  });

  $menuTrigger.click(function(e) {
    e.preventDefault();
    var $this = $(this);
    $this.toggleClass('active').next('ul').toggleClass('active');
  });

    /* setting the default states. */
    // first set an active class on all ULs that are the parent of the current page.
    $('.offcanvas-menu .current-page').parents('nav ul').addClass('active');
    // then expand the .nav-more-link of all currently expanded ULs in the nav.
    $('.offcanvas-menu .current-page').parents('nav ul.active').prev('.nav-more-link').addClass('active');
    // then make the chidren of the current node visible.
    $('.offcanvas-menu .current-page ~ ul').addClass('active');
    // and make the current node + sign a minus
    //$('.offcanvas-menu .current-page').parents('nav ul.active li').find('.nav-more-link').addClass('active');
    $('.offcanvas-menu .current-page + .nav-more-link').addClass('active');
    // if current page has children open the first level
    $('.offcanvas-menu .has-subnav.current-page').children().addClass('active');
});

$("document").ready(function(){
  $(".tab-slider--body").hide();
  $(".tab-slider--body:first").show();
});

$(".tab-slider--nav li").click(function() {
  $(".tab-slider--body").hide();
  var activeTab = $(this).attr("rel");
  $("#"+activeTab).fadeIn();
	if($(this).attr("rel") == "tab2"){
		$('.tab-slider--tabs').addClass('slide-the-tab');
	}else{
		$('.tab-slider--tabs').removeClass('slide-the-tab');
	}
  $(".tab-slider--nav li").removeClass("active");
  $(this).addClass("active");
});



//Slick -- Carousel Instagram
$(document).ready(function() {
  $('.carousel-instagram').slick({
  	slidesToShow: 1,
  	slidesToScroll: 1,
  	arrows: false,
  	autoplay: false,
  	autoplaySpeed: 5000,
  });
});

////////Expanding Side Nav/////////
$(document).ready(function() {
  var $menu = $('.footer-expanding'),
      $menulink = $('.footer-expanding-menu-link'),
      $menuTrigger = $('.footer-expanding-link');
  $menulink.click(function(e) {
    e.preventDefault();
    $menulink.toggleClass('active');
  });
  $menuTrigger.click(function(e) {
    e.preventDefault();
    var $this = $(this);
    $this.toggleClass('active').next('nav').toggleClass('active');
  });
});

// YouTube Video Modal
// Author:  Matt Amyot
// URL: https://codepen.io/mattamyot/pen/dMGwyK?editors=0010
$(document).ready(function(){
	// pass the YouTube video ID into the iframe template on click/tap
	$('a.video-thumb').click(function () {
		// Grab the video ID from the element clicked
		var id = $(this).attr('data-youtube-id');
		// Autoplay when the modal appears
		// Note: this is intetnionally disabled on most mobile devices
		// If critical on mobile, then we need to brainstorm a way, or not use YouTube?
		var autoplay = '?autoplay=1';
		// Don't show the 'Related Videos' when the video ends
		var related_no = '&rel=0';
		// String the ID and param variables together
		var src = '//www.youtube.com/embed/'+id+autoplay+related_no;
		// Set the source on the iframe template inside the video modal
		$("#youtube").attr('src', src);
		return false;
	});
	/* Modal View */
	function toggle_video_modal() {
	    // Open the Video Modal
	    $(".js-trigger-modal").on("click", function(event){
	        event.preventDefault();
	        $("body").addClass("show-video-modal");
	    });
	    // Close and Reset the Video Modal
	    $('body').on('click', '.close-video-modal, .video-modal .overlay', function(event) {
	        event.preventDefault();
	        $("body").removeClass("show-video-modal");
	        // reset the source attribute for the iframe template - kills the video
			$("#youtube").attr('src', '');
	    });
	}
	toggle_video_modal();
});

//Fix z-index youtube video embedding
//http://stackoverflow.com/questions/9074365/youtube-video-embedded-via-iframe-ignoring-z-index
$(document).ready(function (){
    $('iframe').each(function(){
        var url = $(this).attr("src");
        $(this).attr("src",url+"?wmode=transparent");
    });
});

//Scroll to top link
var $toplink = $('#back-to-top');
$toplink.click(function() {
    $('html, body').animate({
        scrollTop: $('body').offset().top
    }, 500);
});




$(document).ready(function() {
	// Show or hide the sticky footer button
	$(window).scroll(function() {
		if ($(this).scrollTop() > 200) {
			$('#back-to-top').fadeIn(200);
		} else {
			$('#back-to-top').fadeOut(200);
		}
	});
	// Animate the scroll to top
	$('#back-to-top').click(function(event) {
		event.preventDefault();
		$('html, body').animate({scrollTop: 0}, 300);
	})
});


//Slick - Carousel Double
$(document).ready(function() {
  $('.carousel-double--img').slick({
  	slidesToShow: 1,
  	slidesToScroll: 1,
  	arrows: false,
  	asNavFor: '.carousel-double--txt',
  	autoplay: true,
  	autoplaySpeed: 3000,
    pauseOnHover: false,
    pauseOnDotsHover: false
  });
  $('.carousel-double--txt').slick({
  	slidesToShow: 1,
  	slidesToScroll: 1,
  	asNavFor: '.carousel-double--img',
  	arrows: false,
  	dots: true,
    pauseOnHover: false,
    pauseOnDotsHover: false
  });
});


//Object Fit Polyfill
$(function () { objectFitImages() });

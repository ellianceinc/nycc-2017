/**
lib contains functions to help the analytics scripts run.
**/
var elliance_lib = function () {
 	var that = {};
 	var my = {};
 	
 	/**
 	cross-browser event listener
 	
 	@param Object element element to attach to
 	@param string type type of event (click, etc)
 	@param function() callback function called..
 	**/
 	that.addListener = function (element, type, callback) {
 		if (element.addEventListener) {
 			element.addEventListener(type, callback);
 		}
 		else if (element.attachEvent) {
 			element.attachEvent('on' + type, callback);
 		}
	}
 	
 	that.isInternal = function(path) {
 		if (path === undefined) {
 			return false;
 		}
 		var p = new RegExp("^#|^(https?):\/\/" + window.location.host+"|^((?!http))", "i");
 		return p.test(path);
 	};
 	
 	that.isExternal = function(path) {
 		return !that.isInternal(path);
 	};
 	
 	that.isDownloadable = function(path) {
 		var p = /\.(7z|aac|arc|arj|asf|asx|avi|bin|csv|docx?|exe|flv|gif|gz|gzip|hqx|jar|jpe?g|js|mp(2|3|4|e?g)|mov(ie)?|msi|msp|pdf|phps|png|ppt|qtm?|ra(m|r)?|sea|sit|tar|tgz|torrent|txt|wav|wma|wmv|wpd|xls|xml|z|zip)$/i;
 		return p.test(path);
 	};
 	
 	that.isMailto = function(path) {
 		var p = /^mailto:/i;
 		return p.test(path);
 	};
 	
 	that.getLinkEvent = function(path) {
 		var action = "Clicks";
 		if (path === undefined) {
 			return null;
 		}
 		if ( that.isMailto(path) ) {
 			return gaEvent("MailTo", action, path);
 		}
 		if ( that.isDownloadable(path) ) {
 			return gaEvent("Downloads", action, path);
 		}
 		if ( that.isExternal(path) ) {
 			return gaEvent("Outbound Link", action, path);
 		}
 		return null;
 	};
 	
 	that.embedTag = function(src) {
        		var tag = document.createElement('script');
        		tag.src = src;
        		var firstScriptTag = document.getElementsByTagName('script')[0];
        		firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    	};
 	
 	return that;
 };
 
 /**
 gaEvent() is an object that is passed to the functions that fire events.
 
 @param string category The event category.
 @param string action The event action.
 @param string label The event label.
 **/
 var gaEvent = function(category, action, label) {
 	var that = {};
 	var my = {};
 	
 	that.category = category;
 	that.action = action;
 	that.label = label;
 	
 	return that;
 }

/**
elliance is the main tracking function. It is turned into eAnalytics after it is declared.

@param jQuery $ This requires jQuery, being passed in when defined.
**/
var elliance = function ($) {
    var that = {};
    var my = {};
    my.engine;
    my.has_gaq = !(typeof _gaq === 'undefined'  || _gaq.length === 0);
    my.lib = elliance_lib();

    that.getPercentOfPageScroll = function () {
        var cur_depth = $(document).scrollTop() + $(window).height();
        if (that.doc_height === undefined) {
            that.doc_height = $(document).height();
        }
        var depth_percent = cur_depth / that.doc_height;
        return Math.round(100 * depth_percent);
    };

    that.fireEvent = function (category, action, label) {
    	if (category === undefined) { return; }
        //console.log(category + " | " + action + " | " + label);
        if (my.has_gaq) {
            _gaq.push(['_trackEvent', category, action, label]);
        }
        else {
            ga('send', 'event', category, action, label);
        }
    };
    that.firePageView = function (pageTitle) {
        if (my.has_gaq) {
            _gaq.push(["_set", "title", document.title + " - " + pageTitle]);
            _gaq.push(['_trackPageview', "/#" + pageTitle]);
           // setting the title after the track for all other page interactions.
           _gaq.push(["_set", "title", document.title]);
        }
        else {
            ga('send', 'pageview', {
            	'title': document.title + " - " + pageTitle
            });
        }
    };
    that.fireLinkVisit = function (href) {
    	console.log(href);
        var category, action, label;
        var e = my.lib.getLinkEvent(href);
        if (e === undefined || e === null) {
        		return;
        }
        that.fireEvent(e.category, e.action, e.label);
    };
    that.fireVideoEvent = function (props) {
        var category = "Video",
            action = props.action,
            label = props.video.attr('id');
        eAnalytics.fireEvent(category, action, label);
    };


    that.onYouTubeIframeAPIReady = function () {
        elliance_track_all_videos();
    };
    that.isElementInViewport = function(element) {
        var rect = element.getBoundingClientRect();
        if (rect.top < 0) {
            return false;
        }
        if (rect.top > $(window).height()) {
            return false;
        }
        return true;
        return (
            rect.top >= 0 &&
            rect.left >= 0 &&
            rect.bottom <= $(window).height() &&
            rect.right <= $(window).width()
            );
    }
    that.scrollEvent = function (depthPercents) {
        if (depthPercents.length === undefined) {
            depthPercents = [depthPercents];
        }
        $(window).on("scroll", function (e) {
            var cur_scroll = that.getPercentOfPageScroll();
            for (var i = 0; i < depthPercents.length; i++) {
                var depthPercent = depthPercents[i];
                if (depthPercent < cur_scroll) {
                    that.fireEvent(
			        "Scrolled To",
			        depthPercent + "%",
			        window.location.pathname);
                    depthPercents.splice(i, 1);
                    // removing 1 so that it continued in the search.
                    i = i - 1;
                }
            }
            // no need to fire this event if nothing is left to check against.
            if (depthPercents.length <= 0) {
                $(window).off(e);
            }
        });
    };
    that.trackSectionView = function () {
        var cur_page = undefined,
            selector = 'section:not(:hidden)',
            $sections = $(selector);

        return function page_view_event() {
            var triggered = [];
            $sections.each(function (index, el) {
                if (that.isElementInViewport(el)) {
                    var elId = el.getAttribute('id');
                    var sectionTitle = $(el).data("title");
                    if (sectionTitle == null || sectionTitle == undefined) {
                        sectionTitle = elId;
                    }
                    triggered.push(sectionTitle);
                }
            });
            // if there isn't anything to trigger, keep the current page.
            if (triggered.length === 0) {
                return;
            }
            // if cur page is the first page in the triggered list, then use that.
            // assuming that the first page is the one that is most visible on the page.
            if (triggered.indexOf(cur_page) === 0) {
                return;
            }
            cur_page = triggered[0];
            that.firePageView(cur_page);
        };
    };
    /**
    Tracks a cycle mapped to the associated value.
    **/
    that.trackCycle = function (cycleName, e) {
        that.fireEvent(cycleName, "Cycled")
    };

    that.trackVideo = function (video) {
        if (video.attr('id') === undefined) {   // set an ID to the ID of the youtube video.
            // code from lunametrics to verify that the iframe is a youtube player.
            if (video.attr('src') === undefined) {
                var vidSrc = "";
            } else {
                var vidSrc = video.attr('src');
            }
            var regex = /h?t?t?p?s?\:?\/\/www\.youtube\.com\/embed\/([\w-]{11})(?:\?.*)?/;
            var matches = vidSrc.match(regex);
            // end of lunametrics code.

            // make sure ?enablejsapi=1 is added to the href. otherwise videos won't track.
            var href = video.attr('src');
            href = (href.indexOf('?') > 0) ? href+'&' : href+'?';
            href +=  "enablejsapi=1";
            video.attr('src', href);

            // TODO: make sure no other IDs exist. If they do, we'll have to find a way to 
            // put in unique IDs.
            if (matches && matches.length > 1) {
                video.attr('id', matches[1]);
            }
            //throw "Video " + video.attr('src') + " does not have an id. The tracker requires IDs exist on the videos.";
        }
        var player = new YT.Player(
            video.attr('id'),
            {
                events: {
                    'onStateChange': stateChangeEventHandler({ video: video })
                }
            }
        );

        /**
        Creates and returns the event function. Uses closures to keep the state
        of the video and it's associated player.
        **/
        function stateChangeEventHandler(props) {
            var video = props.video,
                pause_flag = false;
            //console.log("[video tracker] setting up event tracking.", props);

            return function onPlayerStateChange(event) {
                //console.log("[video tracker] firing event", event.data);
                var action;
                if (event.data == YT.PlayerState.PLAYING) {
                    pause_flag = false;
                    action = "Play";
                }
                else if (event.data == YT.PlayerState.ENDED) {
                    action = "Watch to End";
                }
                else if (event.data == YT.PlayerState.PAUSED && pause_flag == false) {
                    pause_flag = true;
                    action = "Pause";
                }
                else if (event.data == YT.PlayerState.BUFFERING) {
                    action = "Buffering";
                }
                else if (event.data == YT.PlayerState.CUED) {
                    action = "Cueing";
                }
                that.fireVideoEvent({
                    action: action,
                    video: video
                });
            }
        }
    };

    /**
    Runs the standard link tracking that we want.
    **/
    that.setupLinkTracking = function () {
        $('body').on('click', 'a', function (e) {
            var url = $(e.target).attr("href");
            // sometimes, <a>s have children elements that will take the click. Find the
            // parent <a> tag and use that element's @href.
            if (url === undefined || url === null) {
                url = $(e.target).closest('a').attr('href');
            }
            that.fireLinkVisit(url);
        });
    };
    /**
    Runs the standard scrolls that we want.
    **/
    that.setupScrollEvents = function () {
        that.scrollEvent([25, 50, 75]);
        if ($('body').data('track-section-views') == "true") {
            that.trackSectionView()(); // running one time make sure the initial section is tracked.
            $(window).on("scroll", that.trackSectionView());
        }
    };
    /**
    Runs the standard form tracking events that we want.
    **/
    that.setupFormTracking = function () {
        var category = "Form";
        $(window).on('ellianceFormButtonPressed', function (e) {
            var action = "Button Pressed";
            var label = e.formId;
            that.fireEvent(category, action, label);
        });
        $(window).on('ellianceFormSuccess', function (e) {
            var action = "Submit Succeeded";
            var label = e.formId;
            that.fireEvent(category, action, label);
        });
        $(window).on('ellianceFormFail', function (e) {
            var action = "Submit Failed";
            var label = e.formId;
            that.fireEvent(category, action, label);
        });
    };
    /**
    Use this to run all of the jobs on the page.
    **/
    that.setupAll = function () {
        that.setupScrollEvents();
        that.setupLinkTracking();
        that.setupFormTracking();

        my.lib.embedTag("//www.youtube.com/iframe_api");
    };
    /**
    setupVideoTracker sets up all videos on the page to be tracked.
    **/
    that.setupVideoTracker = function (selector) {
        var video_selector = (selector === undefined) ? 'iframe[src*="youtube.com"]' : selector;

        $(video_selector).each(track_this_video);

        function track_this_video() {
            that.trackVideo($(this));
        };
    }

    return that;
};

var eAnalytics = elliance(jQuery);
jQuery(function () {
    eAnalytics.setupAll();
});

/**
Youtube Player API calls this automatically when it loads.
**/
function onYouTubeIframeAPIReady() {
    eAnalytics.setupVideoTracker();
    // now continually watch the page for new events if linkes to youtube videos exist. this handles cases where links are turned into overlays. specific case for venbox jquery plugin.
    if (jQuery('a.venobox_custom').length > 0) {
        setInterval(setupVideoTrackingForVenobox(), 1000);
    }
}

function setupVideoTrackingForVenobox() {
    var locked = false;

    return function () {
        if (!locked && jQuery(".vbox-overlay").length > 0) {
            eAnalytics.setupVideoTracker('.vbox-overlay iframe[src *= "youtube.com"]');
            locked = true;
        }
        if (jQuery('.vbox-overlay').length <= 0) {
            locked = false;
        }
    }
}

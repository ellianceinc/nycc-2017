#!/bin/bash

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
cd $DIR/../

cp README.md
zip -r nycc.zip ./* -x *.git* -x *ci-resources* -x *.zip*

